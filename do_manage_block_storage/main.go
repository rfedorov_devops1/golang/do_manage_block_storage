package main

import (
	"flag"
	"fmt"
)

const (
	blockStorageSizeInGb     = 100
	blockStorageFileSystem   = "ext4"
	doApiVolumesUrl          = "https://api.digitalocean.com/v2/volumes"
	doApiVolumesContentType  = "application/json"
	vagrantVariablesFilePath = "./variables.yml"
)

func main() {
	var doToken, doRegion, action, blockStorageName string
	flag.StringVar(&doToken, "token", "", "DigitalOcean Api Token")
	flag.StringVar(&doRegion, "region", "", "DigitalOcean Region")
	flag.StringVar(&action, "action", "", "What do you want to do")
	flag.StringVar(&blockStorageName, "name", "", "Block Storage Name")
	flag.Parse()

	doClient := DoRequestOption{}
	yamlConfig := VagrantConfig{}

	doClient.SetterDoClient(doToken, doRegion)
	doClient.SetterDoRequest(action, blockStorageName, blockStorageFileSystem, blockStorageSizeInGb, yamlConfig.vagrantGetExistingStorageUuidFromYaml(vagrantVariablesFilePath))

	switch action {
	case "create":
		doClient.createBlockStorage()
		yamlConfig.vagrantModifyFile(vagrantVariablesFilePath, action, doClient.DoGetterBlockStorageUuid())
		fmt.Println("==> Successfully created block storage with ID " + doClient.DoGetterBlockStorageUuid())
	case "delete":
		blkUuid := doClient.DoGetterBlockStorageUuid()
		doClient.deleteBlockStorage()
		yamlConfig.vagrantModifyFile(vagrantVariablesFilePath, action, "")
		fmt.Println("==> Successfully deleted block storage with ID " + blkUuid)
	}
}

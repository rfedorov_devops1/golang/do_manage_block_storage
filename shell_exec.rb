module ShellExec
    variablesFile = YAML.load_file('variables.yml')

    ChangeMountPointScript = <<-SCRIPT
    umount /dev/sda && \
      mkdir -p /srv/data && \
      if [ $(grep "/dev/sda" >/dev/null /etc/fstab; echo $?) -ne 0 ]; then
        echo "/dev/sda /srv/data ext4 defaults 0 1" >> /etc/fstab && mount -av; fi;
    SCRIPT
    
    CreateBlockDeviceScript = <<-SCRIPT
    go run do_manage_block_storage/* \
      --token #{variablesFile['DO_TOKEN']} \
      --region #{variablesFile['DO_DROPLET_REGION']} \
      --action create \
      --name frlteststorage
    SCRIPT
    
    DeleteBlockDeviceScript = <<-SCRIPT
    sleep 15;
    go run do_manage_block_storage/* \
      --token #{variablesFile['DO_TOKEN']} \
      --region #{variablesFile['DO_DROPLET_REGION']} \
      --action delete
    SCRIPT
end
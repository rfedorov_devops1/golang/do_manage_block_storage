package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gopkg.in/yaml.v2"
)

func errorNullVariable(errorMsg string) {
	fmt.Println(errorMsg)
	os.Exit(1)
}

func errorInExecution(errorMsg error) {
	if errorMsg != nil {
		log.Fatal(errorMsg)
	}
}

func (docbs *DoRequestOption) deleteBlockStorage() {
	clientHttp := http.Client{}

	if len(docbs.blockStorageUuid) == 0 {
		fmt.Println("==> Nothing to delete, finishing execution")
		os.Exit(0)
	}

	urlRequest := doApiVolumesUrl + "/" + docbs.blockStorageUuid
	setDeleteVolumeRequest, err := http.NewRequest("DELETE", urlRequest, nil)
	errorInExecution(err)

	setDeleteVolumeRequest.Header = http.Header{
		"Content-Type":  []string{doApiVolumesContentType},
		"Authorization": []string{"Bearer " + docbs.doToken},
	}
	deleteVolumeRequest, err := clientHttp.Do(setDeleteVolumeRequest)
	errorInExecution(err)

	defer deleteVolumeRequest.Body.Close()

	var deleteVolumeResponse DoApiDbsResponseBody
	json.NewDecoder(deleteVolumeRequest.Body).Decode(&deleteVolumeResponse)

	if deleteVolumeRequest.StatusCode != 204 {
		err = errors.New(deleteVolumeResponse.Message)
		errorInExecution(err)
	}

}

func (docbs *DoRequestOption) createBlockStorage() (uuid string) {
	clientHttp := http.Client{}

	requestBody := DoApiCbsRequestBody{docbs.blockStorageSizeInGb, docbs.blockStorageName, docbs.doRegion, docbs.blockStorageFileSystem}
	requestBodyJson, err := json.Marshal(requestBody)
	errorInExecution(err)

	setCreateVolumeRequest, err := http.NewRequest("POST", doApiVolumesUrl, bytes.NewBuffer(requestBodyJson))
	errorInExecution(err)

	setCreateVolumeRequest.Header = http.Header{
		"Content-Type":  []string{doApiVolumesContentType},
		"Authorization": []string{"Bearer " + docbs.doToken},
	}
	createVolumeRequest, err := clientHttp.Do(setCreateVolumeRequest)
	errorInExecution(err)

	defer createVolumeRequest.Body.Close()

	var createVolumeResponse DoApiCbsResponseBody
	json.NewDecoder(createVolumeRequest.Body).Decode(&createVolumeResponse)

	if createVolumeRequest.StatusCode != 201 {
		err = errors.New(createVolumeResponse.Message)
		errorInExecution(err)
	}

	uuid = createVolumeResponse.Volume.BlockStorageUuid
	docbs.blockStorageUuid = uuid
	return uuid
}

func (docl *DoRequestOption) SetterDoClient(doToken, doRegion string) error {
	if len(doToken) == 0 || len(doRegion) == 0 {
		errorNullVariable("Token or region cannot be missing")
	}
	docl.doToken = doToken
	docl.doRegion = doRegion
	return nil
}

func (doreq *DoRequestOption) SetterDoRequest(action, blockStorageName, blockStorageFileSystem string, blockStorageSizeInGb int, blockStorageUuid string) error {
	switch action {
	case "create":
		if len(blockStorageName) == 0 || len(blockStorageFileSystem) == 0 {
			errorNullVariable("Name or FileSytem cannot be missing")
		}
		if blockStorageSizeInGb < 1 {
			errorNullVariable("Size must be greater than 1 GB")
		}
	case "delete":
		doreq.blockStorageUuid = blockStorageUuid
	default:
		errorNullVariable("Unknown action")
	}
	doreq.blockStorageName = blockStorageName
	doreq.blockStorageFileSystem = blockStorageFileSystem
	doreq.blockStorageSizeInGb = blockStorageSizeInGb
	return nil
}

func (doget *DoRequestOption) DoGetterBlockStorageUuid() string {
	return doget.blockStorageUuid
}

func (yamlFile *VagrantConfig) vagrantModifyFile(yamlFilePath string, state string, uuid string) {
	yamlFileRead, err := ioutil.ReadFile(yamlFilePath)
	errorInExecution(err)

	err = yaml.Unmarshal(yamlFileRead, &yamlFile)
	errorInExecution(err)
	switch state {
	case "create":
		yamlFile.DoDiskUuid = uuid
	case "delete":
		yamlFile.DoDiskUuid = uuid
	default:
		errorNullVariable("UUID cannot be missing")
	}

	yamlConfigMarshal, err := yaml.Marshal(yamlFile)

	err = ioutil.WriteFile(yamlFilePath, []byte(yamlConfigMarshal), 0644)
	errorInExecution(err)
}

func (yamlFile *VagrantConfig) vagrantGetExistingStorageUuidFromYaml(yamlFilePath string) (uuid string) {
	yamlFileRead, err := ioutil.ReadFile(yamlFilePath)
	errorInExecution(err)

	err = yaml.Unmarshal(yamlFileRead, &yamlFile)
	errorInExecution(err)
	return yamlFile.DoDiskUuid
}

<div align ="center"><b>Vagrant: creating a droplet on DigitalOcean with an additional 100GB storage</b></div>

[[_TOC_]]

## Description
Vagrantfile allows you to creating a droplet on DigitalOcean with an additional 100GB storage

## System requirements
* ![Installed](https://go.dev/doc/install) version of Golang >= 1.17
* ![Installed](https://www.vagrantup.com/downloads) version of Vagrant >= 2.2.19
    * ![Installed](https://github.com/devopsgroup-io/vagrant-digitalocean#install) DigitalOcean plugin for Vagrant

## Usage example
1. Copy the variables.yml.simple file to the variables.yml file
2. Create ssh key pair and place it in ./.ssh folder
3. Set the variable values ​​in the variables.yml file. 
    * Attention! Don't set the DO_DISK_UUID variable - Vagrant sets the value for this variable
3. Start creating a droplet on DigitalOcean: ```vagrant up --provider=digital_ocean```
4. Delete the droplet: ```vagrant destroy```

## Description of variables
#### DO_DROPLET_SIZE
* Droplet size in DigitalOcean
* Type: ```String```
* Default value: ```s-1vcpu-1gb```

#### DO_DROPLET_REGION
* Region for creating a droplet on DigitalOcean
* Type: ```String```
* Default value: ```nyc1```

#### DO_DROPLET_IMAGE
* The system image from which the droplet will be created on DigitalOcean
* Type: ```String```
* Default value: ```ubuntu-20-04-x64```

#### DO_DROPLET_SSH_KEY_NAME
* Key name for ssh on DigitalOcean
* Type: ```String```
* Default value: ```ubuntu-frl-20.04```

#### DO_DROPLET_SSH_KEY_PATH
* Path to private key for ssh
* Type: ```String```
* Default value: ```./.ssh/vagrant```

#### DO_DROPLET_NAME
* Droplet name on DigitalOcean
* Type: ```String```
* Default value: ```ubuntu-frl-20.04```

#### DO_TOKEN
* DigitalOcean Api access token
* Type: ```String```
* Default value: ```null```

package main

type DoRequestOption struct {
	doToken, doRegion string
	BlockStorage
}

type BlockStorage struct {
	blockStorageUuid, blockStorageName, blockStorageFileSystem string
	blockStorageSizeInGb                                       int
}

type DoApiCbsRequestBody struct {
	SizeGigabytes          int    `json:"size_gigabytes"`
	NameBlockStorage       string `json:"name"`
	Region                 string `json:"region"`
	FileSystemBlockStorage string `json:"filesystem_type"`
}

type DoApiCbsResponseBody struct {
	Id        string `json:"id"`
	Message   string `json:"message"`
	RequestID string `json:"request_id"`
	Volume    struct {
		BlockStorageUuid string `json:"id"`
	} `json:"volume"`
}

type DoApiDbsResponseBody struct {
	Id        string `json:"id"`
	Message   string `json:"message"`
	RequestID string `json:"request_id"`
}

type VagrantConfig struct {
	DoDiskUuid     string `yaml:"DO_DISK_UUID"`
	DoToken        string `yaml:"DO_TOKEN"`
	DoDropletName  string `yaml:"DO_DROPLET_NAME"`
	DoSshKey       string `yaml:"DO_DROPLET_SSH_KEY_PATH"`
	DoSshKeyName   string `yaml:"DO_DROPLET_SSH_KEY_NAME"`
	DoDropletImage string `yaml:"DO_DROPLET_IMAGE"`
	DoRegion       string `yaml:"DO_DROPLET_REGION"`
	DoDropletSize  string `yaml:"DO_DROPLET_SIZE"`
}
